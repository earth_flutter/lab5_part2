import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Retrive text input',
      home: MycustomForm(),
    );
  }
}

class MycustomForm extends StatefulWidget {
  MycustomForm({Key? key}) : super(key: key);

  @override
  _MycustomFormState createState() => _MycustomFormState();
}

class _MycustomFormState extends State<MycustomForm> {
  final myController = TextEditingController();
  @override
  void initState() { 
    super.initState();
    myController.addListener(_printLastestValue);
  }
  @override
  void dispose() { 
    myController.dispose();
    super.dispose();
  }
  void _printLastestValue(){
    print('Second Text field : ${myController.text}');
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Recive Text Input'),
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(children: [
          TextField(
            onChanged: (text) {
              print('First Text field : $text');
            },
          ),
          TextField(
            controller: myController,
          )
        ],),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            showDialog(context: context, builder: (context) {
              return AlertDialog(
                content: Text(myController.text),
              );
            });
          },
          tooltip: 'Show me the value',
          child: Icon(Icons.text_fields),
        ),
    );
  }
}